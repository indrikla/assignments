package assignments.assignment3;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;


public class SimulatorGUI extends Application {
    TextArea addT;
    ArrayList<String> dataRaw = new ArrayList<String>();
    Label resultLB;

    @Override
    public void start(Stage stage) {

        AnchorPane ap = new AnchorPane();
        Scene main = new Scene(ap, 300, 300);
        stage.setTitle("COVID-X | Input Data");
        stage.setResizable(false);

        // Label
        Label label1 = new Label();
        label1.setText("Input your data here:");
        label1.setLayoutX(62);
        label1.setLayoutY(37);

        //Input data
        addT = new TextArea();
        String promptText = "ex: \n ADD OJOL Joko \n POSITIFKAN JOKO";
        addT.setPromptText(promptText);
        addT.setPrefSize(145,100);
        addT.setLayoutX(62);
        addT.setLayoutY(75);

        //Button data
        Button addBT = new Button("DONE");
        addBT.setLayoutX(110);
        addBT.setLayoutY(200);

        // Adding all components into Scene main
        ap.getChildren().addAll(label1, addT, addBT);

        // Scene untuk Result
        VBox ap2 = new VBox();
        ap2.setSpacing(25);
        ap2.setAlignment(Pos.CENTER);
        Scene result = new Scene(ap2, 600, 400);

        // Close button
        Button close = new Button();
        close.setText("CLOSE");

        addBT.setOnAction(event -> {
            stage.setScene(result);
            stage.setTitle("COVID-X | Result");
            stage.setResizable(true);
            InputOutput io = new InputOutput("gui", "", "terminal", "");
            try {
                if(!addT.getText().isEmpty()) {
                    for (String element: addT.getText().split("\n")){
                        dataRaw.add(element);
                    }
                    io.setData(dataRaw);
                    io.run();
                    resultLB = new Label(io.getResult());
                    resultLB.setLineSpacing(2);
                    resultLB.setLayoutX(20);
                    resultLB.setLayoutY(30);
                    ap2.getChildren().addAll(resultLB, close);
                } else {
                    resultLB = new Label("Data cannot be Blank!");
                    resultLB.setLineSpacing(2);
                    resultLB.setLayoutX(20);
                    resultLB.setLayoutY(30);
                    ap2.getChildren().addAll(resultLB, close);
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        });

        close.setOnAction(e -> stage.close());
        stage.setScene(main);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}