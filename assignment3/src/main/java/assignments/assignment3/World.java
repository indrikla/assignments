package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World{

    public List<Carrier> listCarrier = new ArrayList<Carrier>();

    public World(){
        // TODO: Buat constructor untuk class World
        
    }

    public Carrier createObject(String tipe, String nama){
        // TODO: Implementasikan apabila ingin membuat object sesuai dengan parameter yang diberikan
        //printListCarrier();
       // System.out.print(listCarrier);
        if (tipe.equalsIgnoreCase("JURNALIS")) {
            Jurnalis obj = new Jurnalis(nama);
            listCarrier.add(obj);
            return obj;
        } else if (tipe.equalsIgnoreCase("OJOL")) {
            Ojol ojol = new Ojol(nama);
            listCarrier.add(ojol);
            return ojol;
        } else if (tipe.equalsIgnoreCase("PEKERJA_JASA")) {
            PekerjaJasa obj = new PekerjaJasa(nama);
            listCarrier.add(obj);
            return obj;
        } else if (tipe.equalsIgnoreCase("PETUGAS_MEDIS")) {
            PetugasMedis obj = new PetugasMedis(nama);
            listCarrier.add(obj);
            return obj;
        } else if (tipe.equalsIgnoreCase("CLEANING_SERVICE")) {
            CleaningService obj = new CleaningService(nama);
            listCarrier.add(obj);
            return obj;
        } else if (tipe.equalsIgnoreCase("PINTU")) {
            Pintu obj = new Pintu(nama);
            listCarrier.add(obj);
            return obj;
        } else if (tipe.equalsIgnoreCase("PEGANGAN_TANGGA")) {
            PeganganTangga obj = new PeganganTangga(nama);
            listCarrier.add(obj);
            return obj;
        } else if (tipe.equalsIgnoreCase("TOMBOL_LIFT")) {
            TombolLift obj = new TombolLift(nama);
            listCarrier.add(obj);
            return obj;
        } else if (tipe.equalsIgnoreCase("ANGKUTAN_UMUM")) {
            AngkutanUmum obj = new AngkutanUmum(nama);
            listCarrier.add(obj);
            return obj;
        } else {
            System.out.println("Wrong Input!");
            return null;
        }
    }

    public void printListCarrier() {
        for (Carrier element: listCarrier){
            System.out.println(element);
        }
    }

    public Carrier getCarrier(String nama){
        // TODO: Implementasikan apabila ingin mengambil object carrier dengan nama sesuai dengan parameter
        for (Carrier element : listCarrier){
            if (element.getNama().equalsIgnoreCase(nama)){
                return element;
            }
        }
        return null;
    }
}
