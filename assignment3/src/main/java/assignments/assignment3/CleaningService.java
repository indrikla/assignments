package assignments.assignment3;

public class CleaningService extends Manusia{
  		
    private int jumlahDibersihkan;

    public CleaningService(String nama){
        // TODO: Buat constructor untuk CleaningService.
        // Hint: Akses constructor superclass-nya
        super(nama);
    }

    public void bersihkan(Benda benda){
        // TODO: Implementasikan apabila objek CleaningService ini membersihkan benda
        // Hint: Update nilai atribut jumlahDibersihkan
        benda.setPersentaseMenular(0);
        this.jumlahDibersihkan++;
        kurangiKasusAktif(benda);
    }

    public int getJumlahDibersihkan(){
        // TODO: Kembalikan nilai dari atribut jumlahDibersihkan
        return jumlahDibersihkan;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "CLEANING SERVICE " + getNama();
    }

}