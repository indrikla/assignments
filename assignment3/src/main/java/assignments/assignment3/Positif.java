package assignments.assignment3;

public class Positif implements Status {

    public String getStatus() {
        return "Positif";
    }

    public void tularkan(Carrier penular, Carrier tertular) {
        // TODO: Implementasikan apabila object Penular melakukan interaksi dengan
        // object tertular
        // Hint: Handle kasus ketika keduanya benda dapat dilakukan disini
        if (penular instanceof Benda && tertular instanceof Benda) {
            ;
        } else if (penular instanceof Manusia && tertular instanceof Benda) {
            (((Benda) tertular)).tambahPersentase();

            // Jika Presentase Menular benda >= 100, Benda akan berstatus Positif
            if (((Benda) tertular).getPersentaseMenular() >= 100) {
                tertular.ubahStatus("Positif");
                try {
                    // Jika si Penular telah memiliki riwayat rantaiPenular, maka kita
                    // harus menambahkan mereka ke dalam rantaiPenular si Tertular 
                    // sekaligus menambahkan total kasus dan kasus aktif para Penular terdahulu
                    if (penular.getRantaiPenular().size() > 0) {
                        if (!penular.getRantaiPenular().get(0).equals(penular)) {
                            // Menambahkan para carrier dari rantaiPenular si Penular
                            // ke rantaiPenular si Tertular
                            for (Carrier el: penular.getRantaiPenular()) {
                                tertular.getRantaiPenular().add(el);
                                el.tambahTotalKasus();
                                el.tambahAktifKasus();
                            }
                        }
                        tertular.getRantaiPenular().add(penular);
                        penular.tambahAktifKasus();
                        penular.tambahTotalKasus();
                        
                    // Handle jika Penular tidak pernah tertular sebelumnya
                    } else {
                        tertular.getRantaiPenular().add(penular);
                        penular.tambahAktifKasus();
                        penular.tambahTotalKasus();
                    }

                } catch (BelumTertularException e) {
                    ;   
                }

            } else {
                tertular.ubahStatus("Negatif");
            }

            

        } else if (penular instanceof Benda && tertular instanceof Manusia) {
            if (((Benda) penular).getPersentaseMenular() >= 100) {
                tertular.ubahStatus("Positif");
                try {
                    if (penular.getRantaiPenular().size() > 0) {
                        if (!penular.getRantaiPenular().get(0).equals(penular)) {
                        // Menambahkan para carrier dari rantaiPenular si Penular
                        // ke rantaiPenular si Tertular
                            for (Carrier el: penular.getRantaiPenular()) {
                                tertular.getRantaiPenular().add(el);
                                if (!el.equals(penular)) {
                                    el.tambahAktifKasus();
                                    el.tambahTotalKasus();
                                }
                            }
                        } 
                        // Giliran Penular dimasukan kedalam list rantaiPenular si Tertular
                        tertular.getRantaiPenular().add(penular);
                        penular.tambahAktifKasus();
                        penular.tambahTotalKasus();

                    // Handle jika Penular tidak pernah tertular sebelumnya
                    } else {
                        tertular.getRantaiPenular().add(penular);
                        penular.tambahAktifKasus();
                        penular.tambahTotalKasus();
                    }
                } catch (BelumTertularException e) {
                    ;
                }   

            } else {
                tertular.ubahStatus("Negatif");
            }

        } else {
            tertular.ubahStatus("Positif");

            try {
                if (penular.getRantaiPenular().size() > 0){
                    if (!penular.getRantaiPenular().get(0).equals(penular)){
                        // Menambahkan para carrier dari rantaiPenular si Penular
                        // ke rantaiPenular si Tertular
                        for (Carrier el: penular.getRantaiPenular()){
                            tertular.getRantaiPenular().add(el);
                            if (!el.equals(penular)){
                                el.tambahAktifKasus();
                                el.tambahTotalKasus();
                            }
                        }
                    }
                    // Giliran Penular dimasukan kedalam list rantaiPenular si Tertular
                    tertular.getRantaiPenular().add(penular);
                    penular.tambahAktifKasus();
                    penular.tambahTotalKasus();
                
                // Handle jika Penular tidak pernah tertular sebelumnya
                } else {
                    tertular.getRantaiPenular().add(penular);
                    penular.tambahAktifKasus();
                    penular.tambahTotalKasus();
                }

            } catch (BelumTertularException e) {
                ;
            }
        }
    }
}