package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public abstract class Carrier{

    private String nama;
    private String tipe;
    private Status statusCovid = new Negatif();
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular = new ArrayList<Carrier>();

    public Carrier(String nama, String tipe){
        // TODO: Buat constructor untuk Carrier.
        this.nama = nama;
        this.tipe = tipe;
    }

    public String getNama(){
        // TODO : Kembalikan nilai dari atribut nama
        return this.nama;
    }

    public String getTipe(){
        // TODO : Kembalikan nilai dari atribut tipe
        return this.tipe;
    }

    public String getStatusCovid(){
        // TODO : Kembalikan nilai dari atribut statusCovid
        return statusCovid.getStatus();
    }

    public int getAktifKasusDisebabkan(){
        // TODO : Kembalikan nilai dari atribut aktifKasusDisebabkan
        return this.aktifKasusDisebabkan;
    }

    public int getTotalKasusDisebabkan(){
        // TODO : Kembalikan nilai dari atribut totalKasusDisebabkan
        return this.totalKasusDisebabkan;
    }

    public List<Carrier> getRantaiPenular() throws BelumTertularException {
        // TODO : Kembalikan nilai dari atribut rantaiPenular
        if (statusCovid.getStatus().equalsIgnoreCase("Negatif")) {
            throw new BelumTertularException(toString() + " berstatus negatif");
        } else {
            return rantaiPenular;
        }
    }

    public void ubahStatus(String status){
        // TODO : Implementasikan fungsi ini untuk mengubah atribut dari statusCovid
        if (status.equalsIgnoreCase("Positif")){
            statusCovid = new Positif();
        } else {
            statusCovid = new Negatif();
            this.rantaiPenular.clear();
        }
    }

    public void interaksi(Carrier lain){
        // TODO : Objek ini berinteraksi dengan objek lain
        statusCovid.tularkan(this, lain);
    }

    public void addRantaiPenular(Carrier c){
        rantaiPenular.add(c);
    }

    public void kurangiKasusAktif(Carrier lain){
        if (lain.rantaiPenular.size() > 0) {
            for (Carrier el: lain.rantaiPenular){
                el.aktifKasusDisebabkan--;
            }
        }
    }

    public void tambahTotalKasus(){
        this.totalKasusDisebabkan++;
    }
    public void tambahAktifKasus(){
        this.aktifKasusDisebabkan++;
    }

    public abstract String toString();

}
