package assignments.assignment3;

import java.io.*;
import java.util.ArrayList;

public class InputOutput {

    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile;
    private World world;
    private ArrayList<String> data;
    private String result = " ";
    private String inputan = "\n";

    public InputOutput(String inputType, String inputFile, String outputType, String outputFile) {
        // TODO: Buat constructor untuk InputOutput.
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        setBufferedReader(inputType);
        setPrintWriter(outputType);
    }

    public void setBufferedReader(String inputType) {
        // TODO: Membuat BufferedReader bergantung inputType (I/O text atau input
        // terminal)
        try {
            data = new ArrayList<String>();
            if (inputType.equalsIgnoreCase("text")) {
                try {
                    FileReader fr = new FileReader(inputFile);
                    br = new BufferedReader(fr);
                    while (inputan != null) {
                        inputan = br.readLine();
                        data.add(inputan);
                    }
                } catch (FileNotFoundException e) {
                    System.out.println("File not found!");
                }
            } else if(inputType.equalsIgnoreCase("gui")) {
                System.out.println("You're using GUI!");

            } else {
                InputStreamReader isr = new InputStreamReader(System.in);
                br = new BufferedReader(isr);
                while (!inputan.equalsIgnoreCase("exit")) {
                    inputan = br.readLine();
                    data.add(inputan);
                }
            }
        } catch (IOException e) {
            System.out.println("IOException ERROR");
        }
    }

    public void setPrintWriter(String outputType) {
        // TODO: Membuat PrintWriter bergantung inputType (I/O text atau output
        // terminal)
        if (outputType.equalsIgnoreCase("text")) {
            try {
                pw = new PrintWriter(outputFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else if((outputType.equalsIgnoreCase("gui"))){
            ;
        } else {
            pw = new PrintWriter(System.out);
        }
    }

    public void setData(ArrayList<String> x) {
        data = x;
    }

    public String getResult(){
        return result;
    }

    public void run() throws IOException {
        // TODO: Program utama untuk InputOutput, jangan lupa handle untuk IOException
        // Hint: Buatlah objek dengan class World
        // Hint: Untuk membuat object Carrier baru dapat gunakan method CreateObject
        // pada class World
        // Hint: Untuk mengambil object Carrier dengan nama yang sesua dapat gunakan
        // method getCarrier pada class World
        
        world = new World();
        for (int i = 0; i < data.size(); i++) {
            String[] str = new String[3];
            str = data.get(i).split(" ");
            
            if (str[0].equalsIgnoreCase("ADD")) {
                world.createObject(str[1], str[2]);

            } else if (str[0].equalsIgnoreCase("INTERAKSI")) {

                // Agar pada INTERAKSI OBJ1 OBJ2, OBJ1 selalu yang berstatus Positif
                if (world.getCarrier(str[1]).getStatusCovid().equals("Positif") && world.getCarrier(str[2]).getStatusCovid().equals("Negatif")){
                    world.getCarrier(str[1]).interaksi(world.getCarrier(str[2]));
                    
                } else if (world.getCarrier(str[2]).getStatusCovid().equals("Positif") && world.getCarrier(str[1]).getStatusCovid().equals("Negatif")){
                    world.getCarrier(str[2]).interaksi(world.getCarrier(str[1]));

                } else {
                    continue; // Keduanya positif dan negatif
                }

            } else if (str[0].equalsIgnoreCase("RANTAI")) {
                try {
                    // Hanya mencetak yang telah tertular
                    if (world.getCarrier(str[1]).getRantaiPenular().size() != 0){
                        result += ("\n" + "Rantai penyebaran " + world.getCarrier(str[1]).toString() + ": ");

                        // Mengahandle agar tidak terjadi Rantai OJOL Joko: OJOL Joko -> OJOL Joko
                        if (world.getCarrier(str[1]).getRantaiPenular().size() == 1 && world.getCarrier(str[1]).getRantaiPenular().get(0).equals(world.getCarrier(str[1]))){
                            result += (world.getCarrier(str[1]).toString());

                        } else {
                            for (Carrier element : (world.getCarrier(str[1])).getRantaiPenular()) {
                                result += (element.toString());
                                result += (" -> ");
                            }
                            // Print objek pemilik list rantaiPenular
                            result += (world.getCarrier(str[1]).toString());
                        }
                    }
                } catch (BelumTertularException e) {
                    result += "\n" + e.toString();
                }
                
            } else if (str[0].equalsIgnoreCase("POSITIFKAN")) {
                world.getCarrier(str[1]).ubahStatus("Positif");
                (world.getCarrier(str[1])).addRantaiPenular(world.getCarrier(str[1]));

            } else if (str[0].equalsIgnoreCase("AKTIF_KASUS_DARI_OBJEK")) {
                result += ("\n" + world.getCarrier(str[1]).toString() + " telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak "
                         + world.getCarrier(str[1]).getAktifKasusDisebabkan() + " objek");

            } else if (str[0].equalsIgnoreCase("TOTAL_KASUS_DARI_OBJEK")) {
                result += ("\n" + world.getCarrier(str[1]).toString() + " telah menyebarkan virus COVID ke "
                        + world.getCarrier(str[1]).getTotalKasusDisebabkan() + " objek");

            } else if (str[0].equalsIgnoreCase("TOTAL_SEMBUH_PETUGAS_MEDIS")) {
                result += ("\n" + world.getCarrier(str[1]).toString() + " telah menyembuhkan "
                        + ((PetugasMedis) world.getCarrier(str[1])).getJumlahDisembuhkan() + " manusia");

            } else if (str[0].equalsIgnoreCase("SEMBUHKAN")) {
                ((PetugasMedis) world.getCarrier(str[1])).obati(((Manusia) world.getCarrier(str[2])));
                
            } else if (str[0].equalsIgnoreCase("TOTAL_SEMBUH_MANUSIA")) {
                result += ("\n" + "Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: "
                        + Manusia.getJumlahSembuh() + " kasus");

            } else if (str[0].equalsIgnoreCase("BERSIHKAN")) {
                ((CleaningService) world.getCarrier(str[1])).bersihkan((Benda) world.getCarrier(str[2]));

            } else if (str[0].equalsIgnoreCase("TOTAL_BERSIH_CLEANING_SERVICE")) {
                ((CleaningService) world.getCarrier(str[1])).getJumlahDibersihkan();
                result += ("\n" + ((CleaningService) world.getCarrier(str[1])).toString() + " membersihkan "
                        + ((CleaningService) world.getCarrier(str[1])).getJumlahDibersihkan() + " benda");

            } else if (str[0].equalsIgnoreCase("EXIT")) {
                break;

            } else if (str[0] == null) {
                break;
            }
        }

        if (!outputFile.equalsIgnoreCase("NOTHING")) {
            pw.write(result);
        } else {
            pw.print(result);
        }
        pw.close();
    }
}