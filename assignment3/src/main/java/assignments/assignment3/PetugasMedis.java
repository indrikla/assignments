package assignments.assignment3;

public class PetugasMedis extends Manusia {

    private int jumlahDisembuhkan;

    public PetugasMedis(String nama) {
        // TODO: Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(nama);
    }

    public void obati(Manusia manusia) {
        // TODO: Implementasikan apabila objek PetugasMedis ini menyembuhkan manusia
        // Hint: Update nilai atribut jumlahDisembuhkan
        this.jumlahDisembuhkan++;
        tambahSembuh();
        kurangiKasusAktif(manusia);
        manusia.ubahStatus("Negatif");

    }

    public int getJumlahDisembuhkan(){
        // TODO: Kembalikan nilai dari atribut jumlahDisembuhkan
        return jumlahDisembuhkan;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "PETUGAS MEDIS " + getNama();
    }
}