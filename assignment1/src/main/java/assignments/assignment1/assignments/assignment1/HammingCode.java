package assignments.assignment1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;
    static ArrayList<Integer> index_redundEnc = new ArrayList<Integer>();
    static ArrayList<Integer> index_redundDec = new ArrayList<Integer>();
    static ArrayList<Integer> codearr = new ArrayList<Integer>();

    /**
     * Menghitung nilai bit redundan dengan parameter Array dan
     * akan me-return antara 0 atau 1.
     */
    public static int hitungRedundArr(int posisi, int[] temp) {
        int red = posisi + 1; // Parity redundan
        int sum = 0;
        for (int x = posisi; x < temp.length; x += (2 * red)) {
            try {
                for (int y = 0; y < red; y++) {
                    if (temp[x + y] == -1) {
                        continue;
                    } else {
                        sum += temp[x + y];
                    }
                }
            } catch (java.lang.ArrayIndexOutOfBoundsException ex) {
                continue;
            }
        }
        if (sum % 2 == 0) {
            return 0;
        } else {
            return 1;
        }
    }
    
    /**
     * Menghitung nilai bit redundan dengan parameter Array dan
     * akan me-return antara 0 atau 1.
     */
    
    public static int hitungRedundArrList(int posisi, ArrayList<Integer> temp) {
        int red = posisi + 1; // Parity redundan
        int sum = 0;
        for (int x = posisi; x < temp.size(); x += (2 * red)) {
            try {
                for (int y = 0; y < red; y++) {
                    if (temp.get(x + y) == -1) {
                        continue;
                    } else {
                        sum += temp.get(x + y);
                    }
                }
            } catch (java.lang.IndexOutOfBoundsException ex) {
                continue;
            }
        }
        if (sum % 2 == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * Menentukan banyak redundan dari string data yang diinput.
     */

    public static int hitungRed(String biner) {
        int m = biner.length();
        int r = 0;
        while (Math.pow(2,r) < (m + r + 1)) {
            r++;
        }
        return r;
    }

    /**
     * Method untuk menjalankan encode.
     */

    public static String encode(String data) {
        int r = hitungRed(data);

        // Membuat temporary Array
        int z = data.length();
        int[] temp = new int[z + r];

        // Mengisi index redundan dengan -1
        for (int i = 0; i < r; i++) {
            temp[(int) Math.pow(2,i) - 1] = -1;
        }

        // Membuat array yang berisikan index redundan
        int panjang = 0;
        while (panjang < r) {
            index_redundEnc.add((int) Math.pow(2,panjang) - 1);
            panjang++;
        }

        // Memasukan bits dari "data" kedalam array "temp" yang telah terisikan redundan
        int bitstr = 0;
        for (int ind = 0; ind < temp.length; ind++) {
            if (temp[ind] == -1) {
                continue;
            } else {
                /* Bits dalam "data" yang berbentuk string akan diconvert menjadi
                char lalu integer sebelum dimasukkan ke dalam array "temp". */
                char slicechr = data.charAt(bitstr);
                String slice = Character.toString(slicechr);
                bitstr++;
                temp[ind] = Integer.parseInt(slice);
            }
        }

        // Memasukan bits redundan sebenarnya kedalam array "fix"
        int[] fix = temp.clone();
        for (int element: index_redundEnc) {
            try {
                fix[element] = hitungRedundArr(element, temp);
            } catch (ArrayIndexOutOfBoundsException ex) {
                break;
            }
        }

        // ArrayList to String converter
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < fix.length; i++) {
            sb.append(fix[i]);
        } 
        String result = sb.toString();
        return result;
    }

    /**
     * Method untuk menjalankan decode.
     */

    public static String decode(String code) {
        codearr.clear();
        index_redundDec.clear();

        // Membuat ArrayList yang berisikan bits dari string code inputan user
        for (int i = 0; i < code.length(); i++) {
            try {
                char codechr = code.charAt(i);
                String codestr = Character.toString(codechr);
                codearr.add(Integer.parseInt(codestr));
            } catch (java.lang.IndexOutOfBoundsException ex) {
                break;
            }
        }

        int r = hitungRed(code);

        // Membuat array yang berisikan index redundan
        int panjang = 0;
        while (panjang < r) {
            index_redundDec.add((int) Math.pow(2,panjang) - 1);
            panjang++;
        }

        // valueRed berisikan value bits tiap index redundan {0/1/...}
        int[] valueRed = new int[index_redundDec.size()];
        int x = 0;
        for (int element: index_redundDec) {
            try {
                valueRed[x] = codearr.get(element);
                x++;
            } catch (java.lang.IndexOutOfBoundsException ex) {
                break;
            }
        }

        /* Memeriksa apakah terdapat bits error dengan cara
         mengcompare value bits redundan dengan hasil penjumlahan bits setelahnya. */
        int index = 0;
        int y = 0;
        for (int element: index_redundDec) {
            try {
                int checker = hitungRedundArrList(element, codearr);
                if (valueRed[y] != checker) {
                    index += element + 1;
                }
            } catch (java.lang.IndexOutOfBoundsException ex) {
                continue;
            }
        }

        // Menggantikan bits yang error
        if (index > 0) {
            int realindex = index - 1;
            try {
                codearr.set(realindex, 0);
            } catch (java.lang.IndexOutOfBoundsException ex) {
                codearr.set(codearr.size() - 2, 0);
            }            
        }

        // Menghilangkan seluruh bits redundan
        Collections.reverse(index_redundDec);
        for (int element:index_redundDec) {
            try {
                codearr.remove(element);
            } catch (java.lang.IndexOutOfBoundsException ex) {
                continue;
            }
        }

        // ArrayList to String converter
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < codearr.size(); i++) {
            sb.append(codearr.get(i));
        } 
        String result = sb.toString();
    
        return result;
    }

    /**
     * Main program for Hamming Code.
     * @param args unused
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}