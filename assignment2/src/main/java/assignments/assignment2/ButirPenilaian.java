package assignments.assignment2;
import java.text.NumberFormat;
import java.text.DecimalFormat;

public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private final boolean terlambat;
    private NumberFormat formatter = new DecimalFormat("#0.00");     

    public ButirPenilaian(final double nilai, final boolean terlambat) {
        // TODO: buat constructor untuk ButirPenilaian.
        this.nilai = nilai;
        this.terlambat = terlambat;
    }

    public double getNilai() {
        // TODO: kembalikan nilai yang sudah disesuaikan dengan keterlambatan.
        double getNilai = 0.00;

        if (nilai < 0.00) {
            nilai = 0.00;
        }

        if (terlambat) {
            getNilai = nilai - ((double) (nilai*PENALTI_KETERLAMBATAN / 100));
            return (double) Math.round(getNilai * 100) / 100;
        } else {
            return (double) Math.round(nilai * 100) / 100;
        }
    }

    @Override
    public String toString() {
        // TODO: kembalikan representasi String dari ButirPenilaian sesuai permintaan soal.
        double nilaiAkhir = getNilai();
        if (terlambat) {
            return formatter.format(nilaiAkhir) +  " (T)";
        } else {
            return formatter.format(nilai);
        }    
    }
}