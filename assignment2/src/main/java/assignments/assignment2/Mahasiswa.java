package assignments.assignment2;
import java.text.NumberFormat;
import java.text.DecimalFormat;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;
    private NumberFormat formatter = new DecimalFormat("#0.00");

    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        // TODO: buat constructor untuk Mahasiswa.
        // Note: komponenPenilaian merupakan skema penilaian yang didapat dari GlasDOS.
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian;
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        // TODO: kembalikan KomponenPenilaian yang bernama namaKomponen.
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
        KomponenPenilaian result = null;
        for (int i = 0; i<komponenPenilaian.length; i++) {
            if (komponenPenilaian[i].getNama().equals(namaKomponen)) {
                result = komponenPenilaian[i];
            }
        }
        return result;
    }

    public String getNpm() {
        // TODO: kembalikan NPM mahasiswa.
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * 
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A"
            : nilai >= 80 ? "A-"
            : nilai >= 75 ? "B+"
            : nilai >= 70 ? "B"
            : nilai >= 65 ? "B-"
            : nilai >= 60 ? "C+" 
            : nilai >= 55 ? "C" 
            : nilai >= 40 ? "D" 
            : "E";
}

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * 
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    public String rekap() {
        // TODO: kembalikan rekapan sesuai dengan permintaan soal.
        String rekap = "\n";
        double nilaiakhir = 0.0;       
        for (int i = 0; i < komponenPenilaian.length; i++) {
            nilaiakhir += komponenPenilaian[i].getNilai();
            rekap += komponenPenilaian[i].toString() + "\n";
        }
        rekap += "Nilai akhir: " + formatter.format(nilaiakhir) + "\n";
        rekap += "Huruf: " + Mahasiswa.getHuruf(nilaiakhir) + "\n";
        rekap += Mahasiswa.getKelulusan(nilaiakhir);
        return rekap;
    }

    public String toString() {
        // TODO: kembalikan representasi String dari Mahasiswa sesuai permintaan soal.
        return this.npm + " - " + this.nama;
    }

    public String getDetail() {
        // TODO: kembalikan detail dari Mahasiswa sesuai permintaan soal.
        String detail = "";
        double nilaiakhir = 0.0;
        for (int index = 0; index < komponenPenilaian.length; index++) {
            detail += komponenPenilaian[index].getDetail();
            nilaiakhir += komponenPenilaian[index].getNilai();
        }
        detail += "\nNilai akhir: " + formatter.format(nilaiakhir) + "\n";
        detail += "Huruf: " + Mahasiswa.getHuruf(nilaiakhir) + "\n";
        detail += Mahasiswa.getKelulusan(nilaiakhir);
        return detail;
    }

    @Override
    public int compareTo(Mahasiswa other) {
        // TODO: definisikan cara membandingkan seorang mahasiswa dengan mahasiswa
        // lainnya.
        // Hint: bandingkan NPM-nya, String juga punya method compareTo.
        int beda = npm.compareTo(other.getNpm());
        return beda;
    }
}