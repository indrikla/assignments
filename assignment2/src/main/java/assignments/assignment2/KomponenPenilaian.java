package assignments.assignment2;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;


public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;
    private NumberFormat formatter = new DecimalFormat("#0.00");
    private ArrayList<Integer> indx = new ArrayList<>();

    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        // TODO: buat constructor untuk KomponenPenilaian.
        // Note: banyakButirPenilaian digunakan untuk menentukan panjang butirPenilaian saja
        // (tanpa membuat objek-objek ButirPenilaian-nya).
        this.nama = nama;
        this.bobot = bobot;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        // TODO: masukkan butir ke butirPenilaian pada index ke-idx.
        // indx buat butir komponen keberapa (ex: Lab 1, Lab 3)
        butirPenilaian[idx] = butir;
        indx.add(idx + 1);
        Collections.sort(indx);
    }

    public String getNama() {
        // TODO: kembalikan nama KomponenPenilaian.
        return this.nama;
    }

    public double getRerata() {
        // TODO: kembalikan rata-rata butirPenilaian.
        double sum = 0.00;
        int banyakBut = 0;
        for (int i = 0; i < butirPenilaian.length; i++) {
            if (butirPenilaian[i] != null) {
                sum += butirPenilaian[i].getNilai();
                banyakBut++;
            } else {
                continue;
            }
        }
        if (sum != 0.00) {
            return Math.round(((double) sum / banyakBut) * Math.pow(10,2)) / Math.pow(10,2);
        } else {
            return 0.00;
        }
    }

    public double getNilai() {
        // TODO: kembalikan rerata yang sudah dikalikan dengan bobot.
        double rerata = getRerata();
        return Math.round((rerata * bobot / 100) * Math.pow(10, 2)) / Math.pow(10, 2);
    }

    public String getDetail() {
        // TODO: kembalikan detail KomponenPenilaian sesuai permintaan soal.
        String details = "";
        double getnilai = getNilai();
        double rerata = getRerata();
        int indButir = 0;
        details += "\n~~~ " + nama + " (" + bobot + "%) ~~~\n";        
        for (int index = 0; index < butirPenilaian.length; index++) {
            if (butirPenilaian[index] != null) {
                if (butirPenilaian.length == 1){
                    details += nama + ": " + butirPenilaian[index].toString() + "\n";
                } else {
                    details += nama + " " + indx.get(indButir) + ": " + butirPenilaian[index].toString() + "\n";
                    indButir++;
                }
            } else {
                continue;
            }
        }
        if (butirPenilaian.length > 1) {
            details += "Rerata: " + formatter.format(rerata) + "\n";
        }
        details += "Kontribusi nilai akhir: " + formatter.format(getnilai) + "\n";
        return details;
    }

    @Override
    public String toString() {
        // TODO: kembalikan representasi String sebuah KomponenPenilaian sesuai permintaan soal.
        double rerata = getRerata();
        return "Rerata " + nama + ": " + formatter.format(rerata);
    }

}