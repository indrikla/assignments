package assignments.assignment2;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
        // TODO: buat constructor untuk AsistenDosen.
        this.kode = kode;
        this.nama = nama;
    }

    public String getKode() {
        // TODO: kembalikan kode AsistenDosen.
        return this.kode;
    }

    public void addMahasiswa(Mahasiswa mahasiswa2) {
        // TODO: tambahkan mahasiswa ke dalam daftar mahasiswa dengan mempertahankan urutan.
        // Hint: kamu boleh menggunakan Collections.sort atau melakukan sorting manual.
        // Note: manfaatkan method compareTo pada Mahasiswa.
        mahasiswa.add(mahasiswa2); 
        Collections.sort(mahasiswa);
    }
    
    public void removeMahasiswa(Mahasiswa mahasiswa2) {
        mahasiswa.remove(mahasiswa2);
    }

    public Mahasiswa getMahasiswa(String npm) {
        // TODO: kembalikan objek Mahasiswa dengan NPM tertentu dari daftar mahasiswa.
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
        Mahasiswa result = null;
        for (int i = 0; i < mahasiswa.size(); i++) {
            if (mahasiswa.get(i).getNpm().equals(npm)) {
                result = mahasiswa.get(i);
            }
        }  
        return result;
    }

    public String rekap() {
        String rekap = "";
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < mahasiswa.size(); i++) {
            sb.append(mahasiswa.get(i));
            rekap += "\n" + sb.toString() + mahasiswa.get(i).rekap() + "\n" ;
            sb.delete(0, sb.length());
        }
        return rekap;
    }

    public String toString() {
        String result = kode + " - " + nama;
        return result;
    }
}